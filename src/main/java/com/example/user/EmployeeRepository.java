package com.example.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.user.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
