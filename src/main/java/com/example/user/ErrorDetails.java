package com.example.user;

import java.util.Date;
import java.util.List;

public class ErrorDetails{
        //private Date timestamp;
        private String message;
        //private String details;
        private List<String> details;

        /*
        public ErrorDetails(Date timestamp, String message, String details) {
            super();
            this.timestamp = timestamp;
            this.message = message;
            this.details = details;
        }

        public Date getTimestamp() {
            return timestamp;
        }

        public String getMessage() {
            return message;
        }

        public String getDetails() {
            return details;
        }

         */

    public ErrorDetails(String message, List<String> details) {
        this.message = message;
        this.details = details;
    }

    public String getMessage() {
        return message;
    }

    public List<String> getDetails() {
        return details;
    }
}

